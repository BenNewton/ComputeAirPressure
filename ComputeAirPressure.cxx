//------------------------------------------------------------------------------
// simple worklet that uses the provided PointElevation worklet to compute
// the pressure at some arbitrary elevations
//
// this is to both re-familiarize myself with VTK-m and to test vectorization
// on any random worklet
//------------------------------------------------------------------------------

// TBB and CUDA versions will set this variable if they are present
// however if no other versions exist, we fall back to serial
#ifndef VTKM_DEVICE_ADAPTER
#define VTKM_DEVICE_ADAPTER VTKM_DEVICE_ADAPTER_SERIAL
#endif

#include <chrono>
#include <iomanip>
#include <iostream>
#include <string>

#include <stdlib.h>
#include <string.h>

#include <vtkm/Math.h>
#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/DataSetBuilderUniform.h>
#include <vtkm/cont/DataSetFieldAdd.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>
#include <vtkm/worklet/PointElevation.h>

bool writeToFile = false, saveTimes = false;
vtkm::Id SIDE_LENGTH = 257;
std::string fileSuffix;

VTKM_CONT
vtkm::cont::ArrayHandle<vtkm::FloatDefault>
ComputeAirPressure(
    vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::FloatDefault, 3> > pointCoords,
    vtkm::Id numIterations)
{
  // this worklet will estimate the pressure at each point coordinate
  vtkm::worklet::PointElevation elevationWorklet;

  elevationWorklet.SetLowPoint(vtkm::Vec<vtkm::FloatDefault, 3>(0, 0, 0));
  elevationWorklet.SetHighPoint(vtkm::Vec<vtkm::FloatDefault, 3>(0, 0, 2000));
  elevationWorklet.SetRange(101325.0, 77325.0);

  // create a dispatcher for this worklet
  vtkm::worklet::DispatcherMapField<vtkm::worklet::PointElevation>
    elevationDispatcher(elevationWorklet);

  // compute pressure!
  // timing code always takes up way more space than the real code >_>
  vtkm::cont::ArrayHandle<vtkm::FloatDefault> pressure;
  vtkm::Id total = 0;
  vtkm::Id times[numIterations];
  std::cout << "Runs Single Average" << std::endl;
  for(vtkm::Id index = 0; index < numIterations; index++) {
    if(index > 0)
      std::cout << "\r";
    auto s = std::chrono::high_resolution_clock::now();

    // actually run the thing
    elevationDispatcher.Invoke(pointCoords, pressure);

    auto e = std::chrono::high_resolution_clock::now();
    vtkm::Id run =
      std::chrono::duration_cast<std::chrono::microseconds>(e-s).count();
    total += run;
    times[index] = run;
    std::cout << std::setw(4) << index+1 << " ";
    std::cout << std::setw(6) << run/1000.0 << " ";
    std::cout << std::setw(7) << total / (index+1) / 1000.0;
    std::cout << " ms   " << std::flush;
  }
  std::cout << std::endl;

  if(saveTimes) {
    std::string filename = "times_" + std::to_string(SIDE_LENGTH) + 
      "_" + fileSuffix + ".csv";
    std::ofstream outfile(filename);
    for(vtkm::Id index = 0; index < numIterations; index++) {
      outfile << times[index] << '\n';
    }
  }

  return pressure;
}

void printUsage()
{
  std::cerr << "Usage: ComputeAirPressure_SERIAL [-w ";
  std::cerr << "-n <numIterations> ";
  std::cerr << "-s ";
  std::cerr << "-l <length>]";
  std::cerr << std::endl;
}

int main(int argc, char **argv)
{
  int numIterations = 1;
  for(int argi = 1; argi < argc; argi++) {
    if(strcmp(argv[argi], "-w") == 0) {
      writeToFile = true;
    }
    else if (strcmp(argv[argi], "-n") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      numIterations = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-s") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      fileSuffix = argv[++argi];
      saveTimes = true;
    }
    else if (strcmp(argv[argi], "-l") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      SIDE_LENGTH = atoi(argv[++argi]);
      std::cout << "SIDE_LENGTH=" << SIDE_LENGTH << std::endl;
    }
    else {
      std::cerr << "Unknown flag " << argv[argi] << std::endl;
      return 1;
    }
  }

  // array containing the elevation data that will be converted to pressure
  vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::FloatDefault, 3> > points;
  const vtkm::Id ARRAY_LENGTH = SIDE_LENGTH*SIDE_LENGTH*SIDE_LENGTH;
  points.Allocate(ARRAY_LENGTH);

  // populate the array with <x, y, elevation>
  vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::FloatDefault, 3> >::PortalControl
    portal = points.GetPortalControl();
  for(vtkm::Id index = 0; index < ARRAY_LENGTH; index++) {
    portal.Set(index, vtkm::Vec<vtkm::FloatDefault, 3>(
        index % SIDE_LENGTH,
        index / SIDE_LENGTH % SIDE_LENGTH,
        index / SIDE_LENGTH / SIDE_LENGTH % SIDE_LENGTH * 31.25));
    // multiplying by 31.25 moves values into the range 0-2000 (meters)
  }

  // calculate pressure from the elevations
  vtkm::cont::ArrayHandle<vtkm::FloatDefault> pressure;
  pressure = ComputeAirPressure(points, numIterations);

  if(writeToFile) {
    // builder to create a uniform grid
    vtkm::cont::DataSetBuilderUniform dataSetBuilder;
    vtkm::Id3 pointDims(SIDE_LENGTH, SIDE_LENGTH, SIDE_LENGTH);
    vtkm::Id3 cellDims = pointDims - vtkm::Id3(1, 1, 1);

    // create our grid as a tall column shape
    vtkm::cont::DataSet dataSet = dataSetBuilder.Create(
        pointDims, // number of points per dimension
        vtkm::Vec<vtkm::FloatDefault, 3>(-32, -32, 0), // center x&y dims
        vtkm::Vec<vtkm::FloatDefault, 3>(1.0, 1.0, 4.0)); // stretch z-axis
    // the "stretch" is just setting the spacing per axis

    // add to the dataset
    vtkm::cont::DataSetFieldAdd dataSetFieldAdd;
    dataSetFieldAdd.AddPointField(dataSet, "pressure", pressure);

    vtkm::io::writer::VTKDataSetWriter writer("pressure.vtk");
    writer.WriteDataSet(dataSet);
  }

  return 0;
}
